var express=require('express'); /*importamos librerias*/
var app=express();              /*iniciando el framework web*/
var port=process.env.PORT || 3000;
var bodyParser=require('body-parser');
app.use(bodyParser.json());

var baseMLabURL = 'https://api.mlab.com/api/1/databases/lpulidol/collections/';
var mLabAPIKey = "apiKey=x9j5e7ravi1Zl6LMT7JgGH6TIAVJQJLd";
var requestJson = require('request-json');


app.listen(port);
console.log("API escuchando en el puerto "+port);

app.get("/apitechu/v1",
  function(req,res){
      console.log("GET /apitechu/v1");
      res.send({"msg":"Hola prueba"});
    }
  );

app.get("/apitechu/v1/users",
    function(req,res){
        console.log("GET /apitechu/v1/users");
        res.sendFile('usuarios.json',{root:__dirname});
        //var users=require('./usuarios.json');
        //res.send(users);
      }
    );

app.post("/apitechu/v1/users",
  function(req,res){
    console.log("POST apitechu/v1/users");
    //console.log(req.headers);
    console.log("First name es "+req.body.first_name);
    console.log("Last name es "+req.body.last_name);
    console.log("Country es "+req.body.country);

    var newUser={
      "first_name":req.body.first_name,
      "last_name":req.body.last_name,
      "country":req.body.country
    };

    var users=require('./usuarios.json');
    users.push(newUser);
    writeUserDataToFile(users);
    console.log("Usuario guardado con éxito");
    res.send({"msg":"Usuario guardado con éxito"});
  }

);

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req,res){
    console.log("Parámetros");
    console.log(req.params);
    console.log("Query String");
    console.log(req.query);
    console.log("Body");
    console.log(req.body);
    console.log("Headers");
    console.log(req.headers);
  }
);

app.delete("/apitechu/v1/users/:id",
  function(req, res){
    console.log("DELETE apitechu/v1/users/:id");
    //console.log(req.params);
    console.log(req.params.id);

    var users=require('./usuarios.json');
    users.splice(req.params.id-1,1);
    writeUserDataToFile(users);
    console.log("Usuario borrado");
    res.send({"msg":"Usuario borrado"});


  }


);

app.post("/apitechu/v1/login",
  function(req,res){
    console.log("POST /apitechu/v1/login");
    var email=req.body.email;
    var pass=req.body.password;
    console.log("email es " + email);
    console.log("password es " + pass);

    var users=require('./usuarios.json');
    var msg="Login incorrecto";
    for(user of users){
      if(user.email==email&&user.password==pass){
        user.logged=true;
        var id=user.id;
        console.log("Login correcto "+user.id);
        msg="Login correcto";
        writeUserDataToFile(users);
      }
    }
    res.send({"mensaje":msg ,"idUsuario":id});

  }
);



app.post("/apitechu/v1/logout",
  function(req,res){
    console.log("POST /apitechu/v1/logout");
    var id=req.body.id;
    console.log("id"+id);
    var users=require('./usuarios.json');
    var msg="Login incorrecto";
    var flag=0;
    for(user of users){
      if(user.id==id&&user.logged==true){
        msg="Login correcto";
        flag=1;
        delete user.logged;
        writeUserDataToFile(users);
      }

    }
    if(flag==0) id=undefined;
    res.send({"mensaje":msg ,"idUsuario":id});
  }

);

function writeUserDataToFile(data){
  var fs=require('fs');
  var jsonUserData=JSON.stringify(data);
  fs.writeFile("./usuarios.json",jsonUserData,"utf-8",function(err){
    if(err){
      console.log(err);
    }else{
      console.log("Datos escritos en archivo");
    }
  }
);
}


app.get("/apitechu/v2/users",
    function(req,res){
        console.log("GET /apitechu/v2/users");

        var httpClient = requestJson.createClient (baseMLabURL);
        console.log("Cliente HTTP creado");

        httpClient.get("user?" + mLabAPIKey,
          function(err,resMLab,body){
            var response = !err ? body :{
              "msg" : "Error obteniendo usuarios"
            }
            console.log("user?" + mLabAPIKey);
            res.send(response);
          }
      );
      }
);

app.get("/apitechu/v2/users/:id",
    function(req,res){
        console.log("GET /apitechu/v2/users/:id");

        var httpClient = requestJson.createClient (baseMLabURL);
        console.log("Cliente HTTP creado");

        var id = req.params.id;

        var query = 'q={"id":' + id + '}';

        httpClient.get("user?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
          //  var response = !err ? body :{
          //    "msg" : "Error obteniendo usuario"
          //  }
          console.log("user?" + query + "&" + mLabAPIKey);
          var response ={};
          if(err){
            response = {"msg" : "Error obteniendo usuario"}
            res.status(500);
          }else {
            if(body.length>0){
              response = body;
            }else{
              response = {"msg":"Usuario no encontrado"};
              res.status(404);
            }
          }
            res.send(response);
          }
      );
      }
);


app.put("/apitechu/v2/login",
  function(req,res){
    console.log("PUT /apitechu/v2/login");
    var email=req.body.email;
    var pass=req.body.password;
    console.log("email es " + email);
    console.log("password es " + pass);

    var httpClient = requestJson.createClient (baseMLabURL);
    console.log("Cliente HTTP creado");

    var query = 'q={"email":"' + email + '","password":"' + pass + '"}';
      var putBody ='{"$set":{"logged":"true"}}';

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
          console.log(baseMLabURL+"user?" + query + "&" + mLabAPIKey);

          var response ={};
          if(err){
            response = {"msg" : "Error obteniendo usuario"}
            res.status(500);
          }else {
            if(body.length>0){
             response = {"msg":"Usuario logado","id": body[0].id};

              httpClient.put("user?s" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err,resMLab2,body2){

                console.log("todo está de PM "+body);
              });


            }else{
              response = {"msg":"Usuario no encontrado o contraseña incorrecta"};
              res.status(404);
            }
      }
res.send(response);
    }
    );
  }
);

app.put("/apitechu/v2/logout",
  function(req,res){
    console.log("PUT /apitechu/v2/logout");
    //var email=req.body.email;
    var id=req.body.id;
  //  console.log("email es " + email);
    console.log("id es " + id);

    var httpClient = requestJson.createClient (baseMLabURL);
//    console.log("Cliente HTTP creado");

    var query = 'q={"id":' + id + '}';
    var putBody='{"$unset":{"logged":""}}';

    httpClient.get("user?" + query + "&" + mLabAPIKey,
      function(err,resMLab,body){
          console.log(baseMLabURL+"user?" + query + "&" + mLabAPIKey);

          var response ={};
          if(err){
            response = {"msg" : "Error obteniendo usuario"}
            res.status(500);
          }else {
            if(body.length>0){
              response = {"msg":"Usuario deslogado","id": body[0].id};

              httpClient.put("user?s" + query + "&" + mLabAPIKey, JSON.parse(putBody),
              function(err,resMLab2,body2){

                console.log("todo está de PM "+body);
              });
            }else{
              response = {"msg":"Usuario no encontrado o contraseña incorrecta"};
              res.status(404);
            }
      }
res.send(response);
    }
    );
  }
);


app.get("/apitechu/v2/users/:id/accounts",
    function(req,res){
        console.log("GET /apitechu/v2/users/:id/accounts");

        var httpClient = requestJson.createClient (baseMLabURL);
        console.log("Cliente HTTP creado");

        var userid = req.params.id;

        var query = 'q={"USERID":' + userid + '}';

        httpClient.get("cuentas?" + query + "&" + mLabAPIKey,
          function(err,resMLab,body){
          //  var response = !err ? body :{
          //    "msg" : "Error obteniendo usuario"
          //  }

          console.log("cuentas?" + query + "&" + mLabAPIKey);
          var response ={};

          if(err){
            response = {"msg" : "Error obteniendo cuentas"}
            res.status(500);
          }else {
            if(body.length>0){
              response = body;
            }else{
              response = {"msg":"Cuentas no encontradas"};
              res.status(404);
            }
          }
            res.send(response);
          }
      );
      }
);
